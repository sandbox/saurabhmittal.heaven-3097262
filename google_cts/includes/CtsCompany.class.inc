<?php

/**
 * @file
 * CTS company API wrapper class.
 */
module_load_include('php', 'google_cts', "/vendor/autoload");

use Google\Cloud\Talent\V4beta1\CompanyServiceClient;
use Google\Cloud\Talent\V4beta1\Company;

class CtsCompany extends CtsTenant {

  protected $companyID;

  function __construct($project_id, $tenant_id, $company_id) {
    parent::__construct($project_id, $tenant_id);

    $this->setCompanyId($company_id);
  }

  /**
   * Set company ID.
   */
  function setCompanyId($company_id) {
    $this->companyID = $company_id;
  }

  /**
   * List Companies.
   */
  function listCompanies() {
    $companyServiceClient = new CompanyServiceClient();

    if (!empty($this->tenantID)) {
      $formattedParent = $companyServiceClient->tenantName($this->projectID, $this->tenantID);
    }
    else {
      $formattedParent = $companyServiceClient->projectName($this->projectID);
    }

    $pagedResponse = $companyServiceClient->listCompanies($formattedParent);

    $companies = array();
    foreach ($pagedResponse->iterateAllElements() as $responseItem) {
      $companies[] = array(
        'name' => $responseItem->getName(),
        'external_id' => $responseItem->getExternalId(),
      );
    }

    return $companies;
  }

  /**
   * Create Company.
   */
  function createCompany($display_name) {
    $companyServiceClient = new CompanyServiceClient();

    if (!empty($this->tenantID)) {
      $formattedParent = $companyServiceClient->tenantName($this->projectID, $this->tenantID);
    }
    else {
      $formattedParent = $companyServiceClient->projectName($this->projectID);
    }

    $company = new Company();
    $company->setDisplayName($display_name);
    $company->setExternalId($display_name);

    try {
      $response = $companyServiceClient->createCompany($formattedParent, $company);

      $data = array(
        'name' => $response->getName(),
        'display_name' => $response->getDisplayName(),
        'external_id' => $response->getExternalId(),
      );

      return $data;
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex, array(), WATCHDOG_ERROR);
    }
    finally {
      $companyServiceClient->close();
    }
  }

  /**
   *  Get company name. 
   */
  function getCompanyName($company_id) {
    $companyServiceClient = new CompanyServiceClient();

    if (!empty($this->tenantID)) {
      $company_name = $companyServiceClient->companyName($this->projectID, $this->tenantId, $company_id);
    }
    else {
      $company_name = $companyServiceClient->companyWithoutTenantName($this->projectID, $company_id);
    }

    return $company_name;
  }

  /**
   * Delete a company.
   */
  function deleteCompany($company_id) {
    $companyServiceClient = new CompanyServiceClient();

    if (!empty($this->tenantID)) {
      $formattedName = $companyServiceClient->tenantName($this->projectID, $this->tenantID);
    }
    else {
      $formattedName = $companyServiceClient->companyWithoutTenantName($this->projectID, $company_id);
    }

    try {
      $companyServiceClient->deleteCompany($formattedName);

      return array(
        'success' => TRUE,
        'deleted_id' => $company_id,
      );
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex, array(), WATCHDOG_ERROR);
    }
    finally {
      $companyServiceClient->close();
    }
  }

}
