<?php

/**
 * @file
 * CTS Project API wrapper Class.
 */

use Google\Cloud\Talent\V4beta1\Project;

Class CtsProject {

  protected $projectID;

  function __construct($project_id) {
    $this->setProjectId($project_id);
  }

  /**
   * Get Project ID.
   */
  function getProjectId() {
    return $this->projectID;
  }

  /**
   * Set Project ID.
   */
  function setProjectId($project_id) {
    $this->projectID = $project_id;
  }

}
