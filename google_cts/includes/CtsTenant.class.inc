<?php

/**
 * @file
 * CTS tenant class related code file.
 */
module_load_include('php', 'google_cts', "/vendor/autoload");

use Google\Cloud\Talent\V4beta1\TenantServiceClient;
use Google\Cloud\Talent\V4beta1\Tenant;

class CtsTenant extends CtsProject {

  protected $tenantID;

  function __construct($project_id, $tenant_id) {
    parent::__construct($project_id);

    $this->setTenantID($tenant_id);
  }

  /**
   *  Set tenant ID.
   */
  function setTenantID($tenant_id) {
    $this->tenantID = $tenant_id;
  }

  /**
   * Create Tenant for scoping resources, e.g. companies and jobs
   */
  function createTenant($externalId) {
    $tenantServiceClient = new TenantServiceClient();

    $formattedParent = $tenantServiceClient->projectName($this->projectID);
    $tenant = new Tenant();
    $tenant->setExternalId($externalId);

    try {
      $response = $tenantServiceClient->createTenant($formattedParent, $tenant);

      return array(
        'name' => $response->getName(),
        'external_id' => $response->getExternalId(),
      );
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex, array(), WATCHDOG_ERROR);
    }
    finally {
      $tenantServiceClient->close();
    }
  }

  /** List Tenants */
  function listTenants() {
    $tenantServiceClient = new TenantServiceClient();
    $formattedParent = $tenantServiceClient->projectName($this->projectID);

    try {
      // Iterate through all elements
      $pagedResponse = $tenantServiceClient->listTenants($formattedParent);

      $tenants = array();
      foreach ($pagedResponse->iterateAllElements() as $responseItem) {
        $tenants[] = array(
          'name' => $responseItem->getName(),
          'external_id' => $responseItem->getExternalId(),
        );

        return $tenants;
      }
    }
    finally {
      $tenantServiceClient->close();
    }
  }

  /**
   * Get tenant.
   */
  function getTenant($tenant_id) {
    $tenantServiceClient = new TenantServiceClient();
    $formattedName = $tenantServiceClient->tenantName($this->projectID, $tenant_id);

    try {
      $response = $tenantServiceClient->getTenant($formattedName);

      return array(
        'name' => $response->getName(),
        'external_id' => $response->getExternalId(),
      );
    }
    finally {
      $tenantServiceClient->close();
    }
  }

  /**
   *  Delete Tenant
   */
  function deleteTenant($tenant_id) {
    $tenantServiceClient = new TenantServiceClient();
    $formattedName = $tenantServiceClient->tenantName($this->projectID, $tenant_id);

    try {
      $tenantServiceClient->deleteTenant($formattedName);

      return array(
        'success' => TRUE,
        'deleted_id' => $tenant_id,
      );
    }
    finally {
      $tenantServiceClient->close();
    }
  }

}
