<?php

/**
 * @file
 * CTS Jobs API wrapper class.
 */
require drupal_get_path('module', 'google_cts') . '/vendor/autoload.php';

use Google\Cloud\Talent\V4beta1\JobServiceClient;
use Google\Cloud\Talent\V4beta1\Job;
use Google\Cloud\Talent\V4beta1\Job\ApplicationInfo;
use Google\Cloud\Talent\V4beta1\CustomAttribute;
use Google\Cloud\Talent\V4beta1\CompensationInfo;
use Google\Cloud\Talent\V4beta1\CompensationInfo\CompensationEntry;
use Google\Cloud\Talent\V4beta1\CompensationInfo\CompensationRange;
use Google\Type\Money;
use Google\Protobuf\DoubleValue;

class CtsJobs extends CtsCompany {

  function __construct($project_id, $tenant_id, $company_id) {
    parent::__construct($project_id, $tenant_id, $company_id);
  }

  /**
   * Import a job item on CTS.
   *
   * @param array $job_details
   *   Job details to import.
   */
  private function createJob($job_details) {
    $jobServiceClient = new JobServiceClient();

    if (!empty($this->tenantID)) {
      $formattedParent = $jobServiceClient->tenantName($this->projectID, $this->tenantID);
    }
    else {
      $formattedParent = $jobServiceClient->projectName($this->projectID);
    }

    // Prepare job request object.
    $job = $this->prepareJobRequestData($job_details);

    try {
      $response = $jobServiceClient->createJob($formattedParent, $job);

      return $response->getName();
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex . ' Import failed for nid :node_id', array(':node_id' => $job_details->nid), WATCHDOG_ERROR);
    }
    finally {
      $jobServiceClient->close();
    }
  }

  /**
   * Update an existing job details on CTS.
   *
   * @param array $job_details
   * @param string $job_id
   *   CTS job id.
   */
  private function updateJob($job_details, $job_id) {
    $jobServiceClient = new JobServiceClient();

    // Get job existing complete name.
    if (!empty($this->tenantID)) {
      $formattedName = $jobServiceClient->jobName($this->projectID, $this->tenantID, $job_id);
    }
    else {
      $formattedName = $jobServiceClient->jobWithoutTenantName($this->projectID, $job_id);
    }

    // Prepare job data and set existing job id to object.
    $job = $this->prepareJobRequestData($job_details, $formattedName);

    try {
      $response = $jobServiceClient->updateJob($job);

      return $response->getName();
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex . ' Job update failed for nid :node_id', array(':node_id' => $job_details->nid), WATCHDOG_ERROR);
    }
    finally {
      $jobServiceClient->close();
    }
  }

  /**
   * Delete a job from CTS index.
   *
   * @param string $jobId
   *   CTS index id.
   * */
  private function deleteJob($jobId) {
    $jobServiceClient = new JobServiceClient();

    // Get job existing complete name.
    if (!empty($this->tenantID)) {
      $formattedName = $jobServiceClient->jobName($this->projectID, $this->tenantID, $jobId);
    }
    else {
      $formattedName = $jobServiceClient->jobWithoutTenantName($this->projectID, $jobId);
    }

    try {
      $jobServiceClient->deleteJob($formattedName);

      return TRUE;
    }
    catch (Exception $ex) {
      watchdog('google_cts', $ex . ' failed for cts id :cts_id', array(':cts_id' => $jobId), WATCHDOG_ERROR);
    }
    finally {
      $jobServiceClient->close();
    }
  }

  /**
   * Get CTS job item id.
   */
  public function getJobCTSId($job_name) {
    $data = explode('/', $job_name);

    return end($data);
  }

  /**
   * Prepare Job request object.
   *
   * @param object $job_details
   *   Job Node content.
   * @param string $formattedName
   *   In case of update, qualified Job name.
   *
   * @return object Google\Cloud\Talent\V4beta1\Job.
   */
  function prepareJobRequestData($job_details, $formattedName = NULL) {
    $job = new Job();

    // In case of job update, Set existing job name to object.
    // For new job it will auto create job name and return after posting on CTS.
    if (!empty($formattedName)) {
      $job->setName($formattedName);
    }

    // Setup required field values for job.
    // Set job company name.
    $job->setCompany($this->getCompanyName($job_details->company_id));
    // Set job requisition ID.
    $job->setRequisitionId($job_details->requisition_id);
    // Set Job title.
    $job->setTitle($job_details->title);
    // Set Job description.
    $job->setDescription($job_details->description);

    // Set up job location.
    if (!empty($job_details->addresses)) {
      $addresses = $job_details->addresses;
      $job->setAddresses($addresses);
    }

    // Set up job application information.
    if (!empty($job_details->job_application['job_url']) || !empty($job_details->job_application['emails'])) {
      $applicationInfo = new ApplicationInfo();

      // Job URL.
      if (!empty($job_details->job_application['job_url'])) {
        $uris = [$job_details->job_application['job_url']];
        $applicationInfo->setUris($uris);
      }

      // Job Emails.
      if (!empty($job_details->job_application['emails'])) {
        $emails = [$job_details->job_application['emails']];
        $applicationInfo->setEmails($emails);
      }

      $job->setApplicationInfo($applicationInfo);
    }

    // Set up Employment type.
    if (!empty($job_details->employment_type)) {
      $job->setEmploymentTypes([$job_details->employment_type]);
    }

    // Set up Job node language.
    if (!empty($job_details->language)) {
      $job->setLanguageCode($job_details->language);
    }

    // Custom searchable attributes.
    if (!empty($job_details->custom_filterable_attributes)) {
      $custom_attr = array_filter($job_details->custom_filterable_attributes);

      foreach ($custom_attr as $key => $values) {
        $attr = new CustomAttribute();
        $custom_attributes[$key] = $attr->setStringValues($values)->setFilterable(TRUE);
      }

      $job->setCustomAttributes($custom_attributes);
    }

    // Additionally, non searchable field values to display with results.
    if (!empty($job_details->custom_non_filterable)) {
      $custom_attr = array_filter($job_details->custom_non_filterable);

      foreach ($custom_attr as $key => $values) {
        $attr = new CustomAttribute();
        $custom_attributes[$key] = $attr->setStringValues($values)->setFilterable(FALSE);
      }

      $job->setCustomAttributes($custom_attributes);
    }

    // Set up Job Salary.
    if (!empty($job_details->salary)) {
      $compensationInfo = new CompensationInfo();
      $compensationEntry = new CompensationEntry();
      $compensationRange = new CompensationRange();

      if (!empty($job_details->salary['salary_min'])) {
        $min_sal = new Money();
        $min_sal->setUnits($job_details->salary['salary_min']);
        $compensationRange->setMinCompensation($min_sal);
      }

      if (!empty($job_details->salary['salary_max'])) {
        $max_sal = new Money();
        $max_sal->setUnits($job_details->salary['salary_max']);
        $compensationRange->setMaxCompensation($max_sal);
      }

      $compensationEntry->setRange($compensationRange);

      $salaryFreq = new DoubleValue();
      $salaryFreq->setValue(1);
      $compensationEntry->setExpectedUnitsPerYear($salaryFreq);
      $compensationEntry->setType(4); // ANNUALIZED_TOTAL_AMOUNT.
      $compensationInfo->setEntries([$compensationEntry]);
      $job->setCompensationInfo($compensationInfo);
    }

    // Set up job as Featured job.
    if (!empty($job_details->featured)) {
      $job->setPromotionValue($job_details->featured);
    }

    // Set Responsibilities.
    if (!empty($job_details->responsibilities)) {
      $job->setResponsibilities($job_details->responsibilities);
    }

    // Set Qualifications.
    if (!empty($job_details->qualification)) {
      $job->setQualifications($job_details->qualification);
    }

    return $job;
  }

}
