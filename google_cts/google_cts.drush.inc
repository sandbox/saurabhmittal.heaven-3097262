<?php

/**
 * @file
 * Drush command for cloud talent solution functionality.
 */

/**
 * Implements hook_drush_command().
 */
function google_cts_drush_command() {
  $items = array();

  $items['cts-list-tenants'] = array(
    'description' => 'List Tenants exists in Project',
  );

  $items['cts-create-tenant'] = array(
    'description' => 'Create a Tenant in the Google CTS Project',
    'arguments' => [
      'id' => 'Tenant name or ID',
    ],
  );

  $items['cts-delete-tenant'] = array(
    'description' => 'Delete a tenant from the Google CTS Project',
    'arguments' => [
      'id' => 'CTS Tenant ID',
    ],
  );

  $items['cts-list-companies'] = array(
    'description' => 'List companies in Google CTS',
  );

  $items['cts-create-company'] = array(
    'description' => 'Create a company in the Google CTS account',
    'arguments' => [
      'name' => 'Company name',
    ],
  );

  $items['cts-delete-company'] = array(
    'description' => 'Delete a company from the Google CTS account',
    'arguments' => [
      'id' => 'CTS Company ID',
    ],
  );

  return $items;
}

/**
 * Drush callback for "cts-create-tenant".
 * Create a Tenant in project.
 *
 * @param string $tenant_name
 *   Tenant ID or name.
 */
function drush_google_cts_cts_create_tenant($tenant_name) {
  if (empty($tenant_name)) {
    return drush_set_error('Tenant name/id argument is required.');
  }

  $obj = _get_cloud_talent_api_object();
  $details = $obj->createTenant($tenant_name);
  drush_print_r($details);
}

/**
 * Drush callback for "cts-delete-tenant".
 * Delete a tenant from project.
 *
 * @param string $tenant_id
 *   Company id.
 */
function drush_google_cts_cts_delete_tenant($tenant_id) {
  if (empty($tenant_id)) {
    return drush_set_error('Tenant ID argument is required.');
  }

  $obj = _get_cloud_talent_api_object();
  $details = $obj->deleteTenant($tenant_id);
  drush_print_r($details);
}

/**
 * Drush callback for "cts-list-tenants".
 *
 * List all available tenants of project.
 */
function drush_google_cts_cts_list_tenants() {
  $obj = _get_cloud_talent_api_object();
  $details = $obj->listTenants();

  drush_print_r($details);
}

/**
 * Drush callback for "cts-create-company".
 * Create a company for project.
 *
 * @param string $company_name
 *   Company name.
 */
function drush_google_cts_cts_create_company($company_name) {
  if (empty($company_name)) {
    return drush_set_error('Company name argument is required.');
  }

  $obj = _get_cloud_talent_api_object();
  $details = $obj->createCompany($company_name);
  drush_print_r($details);
}

/**
 * Drush callback for "cts-delete-company".
 * Delete a company from project.
 *
 * @param string $company_id
 *   Company id.
 */
function drush_google_cts_cts_delete_company($company_id) {
  if (empty($company_id)) {
    return drush_set_error('Company ID argument is required.');
  }

  $obj = _get_cloud_talent_api_object();
  $details = $obj->deleteCompany($company_id);
  drush_print_r($details);
}

/**
 * Drush callback for "cts-list-companies".
 *
 * List all available companies for project.
 */
function drush_google_cts_cts_list_companies() {
  $obj = _get_cloud_talent_api_object();
  $details = $obj->listCompanies();

  drush_print_r($details);
}
