<?php

/** 
 * @file
 * Cloud talent solution configuration settings.
 */

/**
 * Implements hook_form().
 *
 * CTS job search related configurations.
 */
function google_cts_admin_settings_form($form, $form_state) {
  $form['enable_google_cts_job_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Google Job Search And Sync site jobs to CTS.'),
    '#description' => 'This will handle sync jobs on Google CTS, when any job created, updated or deleted on site.',
    '#default_value' => variable_get('enable_google_cts_job_search'),
  );

  $form['cts_project_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Project ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('cts_project_id'),
  );

  $form['cts_tenant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Tenant ID (Optional)'),
    '#description' => t('If you are manitaining project based on tenants.'
      . ' Otherwise default tenant will created.'),
    '#default_value' => variable_get('cts_tenant_id'),
  );

  $form['cts_company_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Company ID, Default Company Id, if not specified with any job.'),
    '#description' => t('Company name is mandatory to index job on Cloud Talent.'
      . 'If no company provided in job. This Company will assigned to that job object.'),
    '#required' => TRUE,
    '#default_value' => variable_get('cts_company_id'),
  );

  $form['cts_disable_keyword_match'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable keyword match.'),
    '#description' => 'Controls whether to disable exact keyword match. When'
    . ' disable keyword match is turned off, a keyword match returns jobs that'
    . ' do not match given category filters when there are matching keywords.',
    '#default_value' => variable_get('cts_disable_keyword_match'),
  );

  $form['cts_enable_location_broadening'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Location Broadening.'),
    '#description' => 'Controls whether to broaden the search when it produces'
    . ' sparse results. Broadened queries append results to the end of the matching results list.',
    '#default_value' => variable_get('cts_enable_location_broadening'),
  );

  return system_settings_form($form);
}
